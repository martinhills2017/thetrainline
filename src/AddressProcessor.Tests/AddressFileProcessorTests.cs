﻿using System.IO;
using System.Reflection;
using AddressProcessing.Address;
using AddressProcessing.Address.v1;
using NUnit.Framework;

namespace AddressProcessing.Tests
{
    [TestFixture]
    public class AddressFileProcessorTests
    {
        private FakeMailShotService _fakeMailShotService;
        private string _testInputFile = @"\test_data\contacts.csv";

        [SetUp]
        public void SetUp()
        {
            
            _testInputFile = TestContext.CurrentContext.TestDirectory + _testInputFile;

            _fakeMailShotService = new FakeMailShotService();
        }

        [Test]
        public void Should_send_mail_using_mailshot_service()
        {
            var processor = new AddressFileProcessor(_fakeMailShotService);
            processor.Process(_testInputFile);

            Assert.That(_fakeMailShotService.Counter, Is.EqualTo(229));
        }

        internal class FakeMailShotService : IMailShot
        {
            internal int Counter { get; private set; }

            public void SendMailShot(string name, string address)
            {
                Counter++;
            }
        }
    }
}