﻿using System;
using System.IO;

namespace AddressProcessing.CSV
{
    public class CsvWriter :IDisposable
    {
        private readonly StreamWriter _streamWriter;
        
        public CsvWriter(string filename)
        {
            _streamWriter = new StreamWriter(filename);    
        }

        public void Write(params string[] columns)
        {
            string output = "";

            for (int i = 0; i < columns.Length; i++)
            {
                output += columns[i];
                if ((columns.Length - 1) != i)
                {
                    output += "\t";
                }
            }

            _streamWriter.WriteLine(output);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                _streamWriter?.Flush();
                _streamWriter?.Dispose();
            }
        }

        public void Dispose()
        {
            _streamWriter.Flush();
            _streamWriter.Close();
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~CsvWriter()
        {
            Dispose(false);
        }
    }
}
