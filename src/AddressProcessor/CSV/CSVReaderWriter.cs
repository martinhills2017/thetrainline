﻿using System;
using System.IO;

namespace AddressProcessing.CSV
{
    /*
        2) Refactor this class into clean, elegant, rock-solid & well performing code, without over-engineering.
           Assume this code is in production and backwards compatibility must be maintained.
        * Unfortunately this is so badly written, to maintain backward compatibility at this stage wouldn't be a good choice of refactoring and it would be better
        * to replace with a brand new object model. However, following the instructions I will maintain compatibility 
        * The readers and writers have been seperated out into two seperate classes that manage their own streams
        * I would generally do a lot more refactoring here - including changing anything that consumed the old classes, not restricting the columns and 
        * also returning a proper object rather than the strings in column variables. It returns an address so should return address objects and accept 
        * address objects. Or the type could be a generic T that is populated, or has an interface that allows it to process ToString and FromString methods to popualte
        * the concrete object. However, according to the instructions this is all out of scope.
    */

    public class CSVReaderWriter
    {
        private CsvReader _csvReader;
        private CsvWriter _csvWriter;

        [Flags]
        public enum Mode { Read = 1, Write = 2 };

        public void Open(string fileName, Mode mode)
        {
            if (mode == Mode.Read)
            {
                _csvReader = new CsvReader(fileName);
            }
            else if (mode == Mode.Write)
            {
                _csvWriter = new CsvWriter(fileName);
            }
        }

        public void Write(params string[] columns)
        {
            _csvWriter.Write(columns);
        }
        [Obsolete ("This method is deprecated and any usages should be revised as the method achieved nothing. Please use Read(out string column1, out string column2)")]
        public bool Read(string column1, string column2)
        {
            return false;
        }

        public bool Read(out string column1, out string column2)
        {
            return _csvReader.ReadLine(out column1, out column2);
        }

        public void Close()
        {
            _csvWriter?.Dispose();
            _csvReader?.Dispose();
        }
    }
}
