﻿using System;
using System.IO;

namespace AddressProcessing.CSV
{
    public class CsvReader : IDisposable
    {
        private readonly StreamReader _streamReader;

        public CsvReader(string filename)
        {
            _streamReader = new StreamReader(filename);
        }

       
        public bool ReadLine(out string column1, out string column2)
        {

            char[] separator = { '\t' };

            string line = _streamReader.ReadLine();
            string[] columns = line?.Split(separator);

            if (columns?.Length >= 2)
            {
                column1 = columns[0];
                column2 = columns[1];
                return true;
            }

            column1 = null;
            column2 = null;
            return false;
        }
        
        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                _streamReader?.Dispose();
            }
        }

        public void Dispose()
        {
            _streamReader.Close();
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~CsvReader()
        {
            Dispose(false);
        }
    }
}
